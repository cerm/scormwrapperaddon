# README #

Dette er et addon til pipwerks scormwrapper
https://github.com/pipwerks/scorm-api-wrapper

### What is this repository for? ###

* dette addon indeholder en række metoder til at tilgå SCORM 2004 interactions

#### functioner ####

pipwerksAddOn.setInteractionValues(interactionID, keyValuePairs)

pipwerksAddOn.setInteractionValue(interactionID, key, value)

pipwerksAddOn.mapInteractionID(interactionID)

pipwerksAddOn.updateScore()

pipwerksAddOn.getInteractionsAsArray()

pipwerksAddOn.getCorrectInteractions()

pipwerksAddOn.getInteractionsCount()

pipwerksAddOn.getInteractionValue(interactionID, key)