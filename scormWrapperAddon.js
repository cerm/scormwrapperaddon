var pipwerksAddOn = {

  'setInteractionValues': function (interactionID, keyValuePairs) {
    while (keyValuePairs.length > 0) {
      var pair = keyValuePairs.pop();
      this.setInteractionValue(interactionID, pair[0], pair[1]);
    }
  },
  'setInteractionValue': function (interactionID, key, value) {
    var interactionN = this.mapInteractionID(interactionID);
    // console.info('interactionN; ' + interactionN);
    if (interactionN >= 0) {
      pipwerks.SCORM.set('cmi.interactions.' + interactionN + '.id', interactionID);
      pipwerks.SCORM.set('cmi.interactions.' + interactionN + '.' + key, value);
    } else {
      var count = pipwerks.SCORM.get('cmi.interactions._count');
      console.info('count: ' + count);
      pipwerks.SCORM.set('cmi.interactions.' + count + '.id', interactionID);
      pipwerks.SCORM.set('cmi.interactions.' + count + '.' + key, value);
    }
    //pipwerks.SCORM.set('cmi.exit', 'suspend');
    pipwerks.SCORM.data.save();

  },
  'mapInteractionID': function (interactionID) {
    var count = pipwerks.SCORM.get('cmi.interactions._count');
    for (var i = 0; i < count; i++) {
      if (pipwerks.SCORM.get('cmi.interactions.' + i + '.id') == interactionID) {
        // console.info('mapInteractionID; ' + interactionID + ', to index; ' + i);
        return i;
      }
    }
    return -1;
  },
  'updateScore': function () {
    var interactions = this.getInteractionsAsArray();
    var raw = 0;
    var max = 0;
    interactions.forEach(function (e) {
      max += Number(e.weighting);
      if (e.result == 'correct') {
        raw += Number(e.weighting);
      }
    });
    pipwerks.SCORM.set('cmi.score.raw', raw);
    pipwerks.SCORM.set('cmi.score.max', max);
    pipwerks.SCORM.set('cmi.score.scaled', raw / max);

    pipwerks.SCORM.set('cmi.completion_threshold', raw / max);
    if (raw / max == 1) {
      pipwerks.SCORM.set('cmi.completion_status', 'completed');
    } else {
      pipwerks.SCORM.set('cmi.completion_status', 'incomplete');
    }
  },
//Outdated - possibly not in use?
  'getInteractionsAsArray': function () {
    var result = [];
    var count = pipwerks.SCORM.get('cmi.interactions._count');
    for (var i = 0; i < count; i++) {
      console.info('i: ' + i);
      var interaction = {};
      interaction.n = i;
      interaction.id = pipwerks.SCORM.get('cmi.interactions.' + i + '.id');
      interaction.timestamp = pipwerks.SCORM.get('cmi.interactions.' + i + '.timestamp');
      interaction.weighting = pipwerks.SCORM.get('cmi.interactions.' + i + '.weighting');
      interaction.result = pipwerks.SCORM.get('cmi.interactions.' + i + '.result');
      result.push(interaction);
    }
    return result;
  },
  'getCorrectInteractions': function () {
    return $.grep(this.getInteractionsAsArray(), function (e) {
      return e.result == 'correct';
    })
  },
  'getInteractionsCount': function () {
    return pipwerks.SCORM.get('cmi.interactions._count');
  },
  'getInteractionValue': function (interactionID, key) {
    return pipwerks.SCORM.get('cmi.interactions.' + this.mapInteractionID(interactionID) + '.' + key);
  }
};


